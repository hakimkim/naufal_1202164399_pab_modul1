package com.example.android.modul1;

import android.service.autofill.TextValueSanitizer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText edt_panjang, edt_lebar;
    private Button btnHitung;
    private TextView txt_hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Hitung Luas Panjang");

        edt_panjang = (EditText)findViewById(R.id.edt_panjang);
        edt_lebar = (EditText)findViewById(R.id.edt_lebar);
        btnHitung = (Button) findViewById(R.id.btn);
        txt_hasil = (TextView) findViewById(R.id.txt_hasil);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String panjang = edt_panjang.getText().toString().trim();
                String lebar = edt_lebar.getText().toString().trim();

                if (TextUtils.isEmpty(edt_panjang.getText()) || TextUtils.isEmpty(edt_lebar.getText())) {
                    Toast.makeText(MainActivity.this, "Nilai yang anda masukin tidak boleh  kosong coy.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        double p = Double.parseDouble(panjang);
                        double l = Double.parseDouble(lebar);
                        double luas = p * l;
                        txt_hasil.setText("Luas nya adalah = " + luas);
                    } catch (NumberFormatException e) {
                        Toast.makeText(MainActivity.this, "Format yang dimasukkan salah", Toast.LENGTH_SHORT).show();
                    }

                }
            }


        });
}
}